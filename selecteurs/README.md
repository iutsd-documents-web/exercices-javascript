# Sélecteurs
## Sélecteur de type

Ce sélecteur simple permet de cibler les éléments qui correspondent au nom indiqué.

    input

Exemple : **input** permettra de cibler tous les éléments &lt;input&gt;.

## Sélecteur de classe
Ce sélecteur simple permet de cibler les éléments en fonction de la valeur de leur attribut class.

     .nomclasse

Exemple : .active permettra de cibler tous éléments qui possèdent la classe active (comme class="article active dernier").

## Sélecteur d'identifiant
Ce sélecteur simple permet de cibler un élément d'un document en fonction de la valeur de son attribut id. Dans un document, il ne doit y avoir qu'un seul élément pour un identifiant donné.

    #valeurid

Exemple : #toc permettra de cibler l'élément qui possède l'identifiant toc (défini avec un attribut id="toc").

## Sélecteurs d’attribut
Les sélecteurs d'attribut permettent de cibler un élément selon la présence d'un attribut ou selon la valeur donnée d'un attribut.

### Présence :
Cible tous les éléments div qui possèdent un attribut title.

    div[title]

### Égalité
Cible tous les éléments qui possèdent un attribut title dont la valeur est exactement lot.

    div[title="lot"]

### Contient
Cible tous les éléments qui possèdent un attribut title et dont la valeur contient au moins une occurrence de lot (contient) dans la chaîne de caractères.

    div[title*="lot"]

### Contient le mot
Permet de cibler un élément qui possède un attribut title dont la valeur est lot. Cette forme permet de fournir une liste de valeurs, séparées par des blancs, à tester. Si au moins une de ces valeurs est égale à celle de l'attribut, l'élément sera ciblé.

    div[title~="lot"]

### Contient
Permet de cibler un élément qui possède un attribut title dont la valeur est exactement lot ou dont la valeur commence par lot suivi immédiatement d'un tiret (U+002D) lot-. Cela peut notamment être utilisé pour effectuer des correspondances avec des codes de langues.

    div[title|="lot"]

### Commence
    div[title^="lot"]

### Termine
    div[title$="lot"]

### Sensibilité à la casse
div[title$="lot"i]
div[title$="lot"s]
i : insensible ; s : sensible

Attribuer la classe selection
1. à tous les éléments de type rect. 2 méthodes sont possibles
2. à l’élément machine41664. 2 méthodes sont possibles
3. à l’élément machine41664 et à l’élément machine20125. 1 seule méthode est possible.
4. à tous les éléments contenant la classe indisponible. 2 méthodes sont possibles
5. à tous les éléments contenant l’attribut data-inventaire.
6. à tous les éléments rect de largeur 35.
7. à tous les éléments de classe machine et de fabricant Billon.
8. à tous les éléments de class stock qui contiennent des vis.
9. à tous les éléments de class stock qui contiennent des articles contenant les lettres vis.
10. à tous les éléments dont la date d’inventaire commence par 2019.
11. à tous les éléments moulage dans la zoneB.
12. à tous les éléments moulage indisponible dans la zoneC.
13. à tous les éléments stock qui n’ont pas de date d’inventaire.
14. le premier élément de la zoneA.
15. le dernier élément de la zoneB.
16. les éléments enfants de l’IlotC.
17. les éléments immédiatement enfants de l’IlotC.
18. les éléments stock qui suivent un élément machine injection.
19. les éléments machine moulage dans le même ilot qu’une machine injection indisponible..
21. la seule machine de son groupe.
22. les machines de l’Ilot A qui ne sont pas indisponible
23. Les stocks avec une quantité supérieur à 30
24. Les stocks avec date d’inventaire supérieure à 12 août 2020
25. le groupe vide dans l’IotC et déplacer les machines de moulage de l’IlotC à l’intérieur
