# Tableaux
## Tri

    Array.sort(__function expression__)

La fonction **sort()** crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine triés. Le tir s'éffectue suivant le résultat de la fonction définie en paramètres et ontenant une expresion de fonction .

La fonction expression prend en paramètres 2 éléments du tableau et doit retourner un nombre négatif si le premier élément est avant le deuxième,
0 si les 2 élements sont au même rang
et positif si le premier élément est après le deuxième.

    tableau.sort((eltA, eltB) => eltA - eltB)

Le résultat de la soustraction correspond bien au résultat que l'on veut pour le tri.

## Filtre sur un tableau

    Array.filter(__expression function__)

La méthode **filter()** crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui remplissent une condition déterminée par la fonction expression.

## Sélection

    Array.slice(debut, fin);

Retourne une sous sélection du tableau.

## Tests sur un tableau

### La condition est-elle remplie au moins une fois

    Array.some(__expression function__);

### La condition est-elle satisfaite pour tous les éléments

    Array.every(__expression function__);

## Map, Filter, Reduce

Map, Filter, Reduce est un modèle de conception (pattern) qui implémente des fonctions qui opèrent sur des séquences de données.

**Map** applique une fonction unaire à chaque élément de la séquence et renvoie une nouvelle séquence contenant les résultats, dans le même ordre.

    Array.map(__expression function__);

**Reduce** combine les éléments de la séquence ensemble, en utilisant une fonction binaire. Un accumulateur est passé d'éléments en éléments qui permet d'ajouter (accumuler) les valeurs une à une. Le résultat final est une seule et unique valeur.

    Array.reduce(__expression function__);

L'intérêt de ces fonctions est de pouvoir les chainer et combiner pour effectuer en une seule ligne plusieurs opération.

    Array.filter(__expression function__).map(expression).reduce(__expression function__);

Utilisation de reduce pour sélectionner une ligne suivant un critère

    Array.reduce(function (select, current) {
  return current > select ? current : bts;
});





## Exercice

### Données
Étudiants inscrits dans chaque région en fonction du diplôme

|code|nom|BTS|DUT|MES|Ingénieur|
|----|---|---|---|---|---------|
|11|Île-de-France|44049|17093|28830|42347|
|24|Centre-Val de Loire|8324|4511|5171|3071|
|27|Bourgogne-Franche-Comté|10102|5012|5180|4899|
|28|Normandie|11885|7011|6471|6497|
|32|Hauts-de-France|26320|10611|14485|16115|
|44|Grand Est|21352|12778|12215|14102|
|52|Pays de la Loire|16704|	5716|	5780|	10292|
|53|Bretagne|	15467|6653|5835|8853|
|75|Nouvelle-Aquitaine|21606|10082|11289|8718|
|76|Occitanie|24394|10578|9980|14641|
|84|Auvergne-Rhône-Alpes|28795|17320|14938|23272|
|93|Provence-Alpes-Côte d'Azur|17893|7780|10871|5774|

#### couleurs

Outil permettant de générer des palettes de couleurs pour les cartes
https://colorbrewer2.org/#type=diverging&scheme=PiYG&n=10

#8e0152 de  3000 à  5000
#c51b7d de  5000 à  6000
#de77ae de  6000 à 10000
#f1b6da de 10000 à 12000
#fde0ef de 12000 à 14000
#e6f5d0 de 14000 à 16000
#b8e186 de 16000 à 18000
#7fbc41 de 18000 à 24000
#4d9221 de 24000 à 30000
#276419 de 30000 à 45000



Proposer un modèle pour stocker les données du tableau
Proposer une méthode pour résoudre le problème

Ajouter un sélecteur &lt;select&gt; pour sélectionner le type de diplôme

Tier et afficher le tableau par rapport à l'effectif en DUT

    data.sort((regionA, regionB) => regionA.dut - regionB.dut)

Afficher les régions qui ont moins d'étudiants en école d'ingénieur qu'en DUT

    data.filter((region) => region.ing < region.dut)

Sélectionner les 3 régions qui comptes le plus d'étudiants en MES
Combiner les fonctions de tri et de sélection pour arriver au résultat

    data.sort((regionA, regionB) => regionB.mes - regionA.mes)
        .slice(0, 3));

Existe-t-il au moins une région qui compte moins de 5 000 étudiants en école d'ingénieur ?

    data.some((region) => region.ing < 5000);

Est-ce que toutes les régions ont bien au moins 5 000 étudiants inscrits en DUT ?

    data.every(region => region.dut > 5000);

Ajouter une colonne aux données qui calcule le nombre d'étudiants total dans chaque région.

    data.map(region => elt.total = elt.dut + elt.dut + elt.mes + elt.ing)

Calculer le nombre total d'étudiants

    data.reduce((accumulator, currentValue) => accumulator + currentValue);

Sélectionner la région qui possède le plus d'étudiant en BTS. Utiliser la fonction reduce pour effecuer la sélection

    data.reduce((accumulator, currentValue) => currentValue.bts > accumulator.bts ? currentValue : accumulator)

Calculer le nombre total d'étudiants en comptabilisant uniquement les régions qui possède plus de 6000 étudiants en MES.

    data.filter(region => region.mes > 6000)
        .map(region => elt.total = elt.dut + elt.dut + elt.mes + elt.ing)
        .reduce((accumulator, currentValue) => accumulator + currentValue);
